package monteCarlo;

/**
 * Created by Jie on 12/11/14.
 */
public interface PayOut {

    public double getPayOut( StockPath path );

}
