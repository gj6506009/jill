package monteCarlo;

import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;
import static org.bridj.Pointer.allocateFloats;

/**
 * Created by Jie on 12/12/14.
 */
public class GPUNormalRandomVectorGenerator implements RandomVectorGenerator {

    // Length of the random number vector
    private int N;
    // Batch size
    private int batchN;
    // Store the normal random vector in GPU to generate Gaussian
    private double[] normalVector;
    // Tracks the number of Gaussian variables assigned
    private int idx;

    /**
     * Constructor
     */
    public GPUNormalRandomVectorGenerator( int N, int batchN ){
        this.N = N;
        this.batchN = N;
        normalVector = getGaussian(this.batchN);
        idx = 0;
    }

    /**
     * This function generates a vector of normal random numbers with a length of N
     * @return an array of double normal random numbers
     */
    public double[] getVector(){
        double[] vector = new double[N];
        for(int i = 0; i < N; i++){
            if(idx == 2*batchN - 1) {
                normalVector = getGaussian(this.batchN);
            }
            vector[i] = normalVector[idx];
            idx++;
        }
        return vector;
    }

    public double[] getGaussian( int batchN ){
        idx = 0;
        // Creating the platform which is out computer.
        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
        // Getting the GPU device
        CLDevice device = clPlatform.getBestDevice();
        // Let's make a context
        CLContext context = JavaCL.createContext(null, device);
        // Lets make a default FIFO queue.
        CLQueue queue = context.createDefaultQueue();
        // Read the program sources and compile them :
        String src = "__kernel void fill_in_values(__global const float* a, __global const float* b, __global float* out1, __global float* out2, float pi, int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n)\n" +
                "        return;\n" +
                "\n" +
                "    out1[i] = sqrt(-2*log(a[i]))*cos(2*pi*b[i]);\n" +
                "    out2[i] = sqrt(-2*log(a[i]))*sin(2*pi*b[i]);\n" +
                "}";

        CLProgram program = context.createProgram(src);
        program.build();
        CLKernel kernel = program.createKernel("fill_in_values");

        final int n = batchN;
        final Pointer<Float>
                aPtr = allocateFloats(n),
                bPtr = allocateFloats(n);
        double[] uniformVector = (new UniformRandomVectorGenerator(2*batchN)).getVector();
        // Generate uniform sequence and assign to aPtr and bPtr
        for (int i = 0; i < n; i++) {
            aPtr.set(i, (float) uniformVector[2*i]);
            bPtr.set(i, (float) uniformVector[2*i+1]);
        }

        // Create OpenCL input buffers (using the native memory pointers aPtr and bPtr) :
        CLBuffer<Float>
                a = context.createFloatBuffer(CLMem.Usage.Input, aPtr),
                b = context.createFloatBuffer(CLMem.Usage.Input, bPtr);

        // Create an OpenCL output buffer :
        CLBuffer<Float>
                out1 = context.createFloatBuffer(CLMem.Usage.Output, n),
                out2 = context.createFloatBuffer(CLMem.Usage.Output, n);

        kernel.setArgs(a, b, out1, out2, (float) Math.PI, batchN);

        CLEvent event = kernel.enqueueNDRange(queue, new int[]{n});
        event.invokeUponCompletion(new Runnable() {
            @Override
            public void run() {

            }
        });
        final Pointer<Float> c1Ptr = out1.read(queue,event);
        final Pointer<Float> c2Ptr = out2.read(queue,event);

        double[] normalVector = new double[2*batchN];
        for(int i = 0; i < batchN; i++){
            normalVector[2*i] = (double) c1Ptr.get(i);
            normalVector[2*i+1] = (double) c2Ptr.get(i);
        }
        return normalVector;
    }

}
