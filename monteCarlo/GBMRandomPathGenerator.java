package monteCarlo;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jie on 12/11/14.
 */
public class GBMRandomPathGenerator implements StockPath{

    private double rate;
    private double sigma;
    private double S0;
    private int N;
    private DateTime startDate;
    private DateTime endDate;
    private RandomVectorGenerator rvg;

    /**
     * Constructor
     *
     * @param rate the risk free rate
     * @param N the number of periods
     * @param sigma the volatility
     * @param S0 current stock price
     * @param startDate option pricing date
     * @param endDate option maturity
     * @param rvg normal random number generator
     */
    public GBMRandomPathGenerator(double rate, int N,
                                  double sigma, double S0,
                                  DateTime startDate, DateTime endDate,
                                  RandomVectorGenerator rvg){
        this.startDate = startDate;
        this.endDate = endDate;
        this.rate = rate;
        this.S0 = S0;
        this.sigma = sigma;
        this.N = N;
        this.rvg = rvg;
    }

    /**
     * Implements StockPath getPrices method
     *
     * @return  list of Pair of DateTime and stock prices, ordered by DateTime
     */
    public List<Pair<DateTime,Double>> getPrices() {

        double[] n = rvg.getVector();
        DateTime current = new DateTime(startDate.getMillis());
        long delta = (endDate.getMillis() - startDate.getMillis())/N; //divided into 252 periods
        //(t(i) - t(i-1))

        double deltaT = (double)delta/ DateTimeConstants.MILLIS_PER_DAY; //how many days per period
        List<Pair<DateTime, Double>> pricePath = new LinkedList<Pair<DateTime,Double>>();
        pricePath.add(new Pair<DateTime, Double>(current, S0));
        for ( int i=1; i < N; ++i){
            //Update current DateTime: add delta to current
            current = current.plusMillis((int) delta);
            pricePath.add(new Pair<DateTime, Double>(current,
                    pricePath.get(pricePath.size()-1).getValue()*Math.exp((rate-sigma*sigma/2)*deltaT+sigma*Math.sqrt(deltaT)*n[i-1])));
        }
        //Sort pricePath by date
        Comparator<Pair<DateTime,Double>> comparator = Pair.getDateComparator(SortOrder.ASCENDING);
        Collections.sort(pricePath, comparator);

        return pricePath;
    }

}
