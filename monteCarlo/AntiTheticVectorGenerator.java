package monteCarlo;

import java.util.Arrays;

/**
 * Created by Jie on 12/11/14.
 */
public class AntiTheticVectorGenerator implements RandomVectorGenerator{

    private RandomVectorGenerator rvg;
    double[] lastVector; //not private, can be accessed and changed

    public AntiTheticVectorGenerator(RandomVectorGenerator rvg){
        this.rvg = rvg;
    }

    public double[] getVector() {
        if ( lastVector == null ){
            lastVector = rvg.getVector();
            return lastVector;
        } else {
            double[] tmp = Arrays.copyOf(lastVector, lastVector.length);
            lastVector = null;
            for (int i = 0; i < tmp.length; ++i){ tmp[i] = -tmp[i];}
            return tmp;
        }
    }

}
