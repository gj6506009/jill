package monteCarlo;

/**
 * Created by Jie on 12/11/14.
 */
public interface RandomVectorGenerator {

    public double[] getVector();

}
