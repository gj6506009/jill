package monteCarlo;

import org.joda.time.DateTime;

import java.util.Comparator;

/**
 * Created by Jie on 12/12/14.
 */
public class Pair<K,V> {

    private K k;
    private V v;

    public Pair(K key, V value){
        k = key;
        v = value;
    }

    public K getKey(){ return k;}
    public V getValue(){ return v;}


    /**
     * Static factor method for instantiating a comparator that will compare Pair by date
     *
     * @param sortOrder DESCENDING or ASCENDING
     * @return 1, 0, -1 depending on which date is greater
     */
    public static Comparator<Pair<DateTime,Double>> getDateComparator( final SortOrder sortOrder ) {
        Comparator<Pair<DateTime,Double>> comparator = new Comparator<Pair<DateTime,Double>>() {
            private int _comparisonMultiplier = sortOrder.getComparisonMultiplier();
            //@override
            public int compare(Pair<DateTime, Double> p1, Pair<DateTime, Double> p2) {
                if ( p1.getKey().isAfter(p2.getKey()) )
                    return 1 * _comparisonMultiplier;
                if ( p1.getKey().isBefore(p2.getKey()) )
                    return -1 * _comparisonMultiplier;
                return 0;
            }
        };
        return comparator;
    }
}
