package monteCarlo;

import org.joda.time.DateTime;

/**
 * Created by Jie on 12/12/14.
 */
public class SimulationManager {

    private double C;
    private PayOut payout;
    private int times;

    public SimulationManager( PayOut p ) {
        payout = p;
    }

    public double simulate(double S0, double rate, double sigma,
                           int N, int batchN, double prob, double threshold,
                           DateTime startDate, DateTime endDate) {

        // Generate stock paths
        GPUNormalRandomVectorGenerator gpu = new GPUNormalRandomVectorGenerator(N, batchN);
        AntiTheticVectorGenerator atRvg = new AntiTheticVectorGenerator(gpu);
        GBMRandomPathGenerator gbm = new GBMRandomPathGenerator( rate, N, sigma, S0, startDate, endDate, atRvg);

        // Statistics collector
        StatsCollector Stats = new StatsCollector();

        // Simulating
        while( true ) {
            Stats.addValue(payout.getPayOut(gbm));
            if ( Stats.getSimulationN() > 100 && Stats.getError(prob) <= threshold ) break;
        }

        C = Math.exp(-rate * N) * Stats.getAverage(); // option price
        times = Stats.getSimulationN(); // times of simulations

        return C;

    }

    public double getPrice(){ return C;}

    public int getTimes(){ return times;}

    public PayOut getPayout() { return payout; }

    public static void main( String[] args ){

        // Current market conditions
        double Ke = 165; //strike price for the European option
        double Ka = 164; //strike price for the Asian option
        double rate = 0.0001;
        double sigma = 0.01;
        double S0 = 152.35;
        int N = 252;
        int batchN =  1024 * 256 * 16;
        double prob = 0.96;
        double threshold = 0.01;
        DateTime startDate = new DateTime();
        DateTime endDate = startDate.plusDays( 252 );

        // Construct European and Asian option object
        EuropeanCallOption EC = new EuropeanCallOption( Ke );
        AsianCallOption AC = new AsianCallOption( Ka );

        SimulationManager manager1 = new SimulationManager(EC);
        final long startTime1 = System.currentTimeMillis();
        double C1 = manager1.simulate(S0, rate, sigma, N, batchN, prob, threshold, startDate, endDate);
        final long endTime1 = System.currentTimeMillis();
        System.out.println("Total execution time of European option: " + (endTime1 - startTime1) );
        System.out.println( "Simulated European Option Price is " + C1 + ".");

        SimulationManager manager2 = new SimulationManager(AC);
        final long startTime2 = System.currentTimeMillis();
        double C2 = manager2.simulate(S0, rate, sigma, N, batchN, prob, threshold, startDate, endDate);
        final long endTime2 = System.currentTimeMillis();
        System.out.println("Total execution time of Asian option: " + (endTime2 - startTime2) );
        System.out.println( "Simulated Asian Option Price is " + C2 + ".");

    }

}
