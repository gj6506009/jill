package monteCarlo;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by Jie on 12/11/14.
 */
public class AsianCallOption implements PayOut {

    private double K;

    public AsianCallOption(double K) {
        this.K = K;
    }

    public double getPayOut(StockPath path) {

        List<Pair<DateTime, Double>> pricePath = path.getPrices();
        Double sum = 0.0;
        for (int i = 0; i < pricePath.size(); i++) {
            sum += pricePath.get(i).getValue();
        }
        Double avg = sum / pricePath.size();

        return Math.max(0, avg - K);
    }

}
