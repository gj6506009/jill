Jie Gao jg3908 N11849027

1. Open https://gj6506009@bitbucket.org/gj6506009/jill.git
2. Go to Downloads, and download this repository.
3. Open Intellij and create a new Maven project with all the copied files. 
4. Open the "SimulationManager.java" and run the main in this file. The results will show in the console. 

Explanation:
Two new classes were created in addition to the previous option pricing homework. 
(1) UniformRandomVectorGenerator.java generates uniform random numbers and implements the RandomVectorGenerator interface. 
(2) GPUNormalRandomVectorGenerator.java takes a uniform vector generated from the above class to the best device, and form a vector of normal random numbers.
