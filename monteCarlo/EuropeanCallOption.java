package monteCarlo;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by Jie on 12/11/14.
 */
public class EuropeanCallOption implements PayOut {

    private double K;

    public EuropeanCallOption(double K){
        this.K = K;
    }

    public double getPayOut(StockPath path) {
        List<Pair<DateTime, Double>> pricePath = path.getPrices();
        return Math.max(0, pricePath.get(pricePath.size()-1).getValue() - K);
    }

}
