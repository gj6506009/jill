package monteCarlo;

import org.apache.commons.math3.distribution.NormalDistribution;

/**
 * Created by Jie on 12/11/14.
 */
public class StatsCollector {

    private double average;
    private double sigma;
    private int num; //number of simulations
    private double error;
    //static LinkedList<Double> collector;
    double sum = 0.; //temporary variable for calculation
    double sumSq = 0.; //temporary variable for calculation

    public StatsCollector() {
        //collector = new LinkedList<Double>();
        num = 0;
        sigma = 0.0;
        average = 0.0;
    }

    public void addValue(Double value) {

        //collector.addLast(value);
        num += 1;
        sum += value;
        average = sum/num;
        sumSq = sumSq + value*value;
        sigma = Math.sqrt((sumSq/num - average*average));

    }

    public double getAverage() {
        return average;
    }

    public double getSigma() {
        return sigma;
    }

    public int getSimulationN() {
        return num;
    }

    public double getError( double prob ) {
        NormalDistribution Y = new NormalDistribution();
        double y = Y.inverseCumulativeProbability(prob);
        error = y * sigma / Math.sqrt(num);
        return error;
    }

}
