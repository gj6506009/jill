package monteCarlo;

import java.util.List;
import org.joda.time.DateTime;

/**
 * Created by Jie on 12/11/14.
 */
public interface StockPath {

    public List<Pair<DateTime,Double>> getPrices();

}
